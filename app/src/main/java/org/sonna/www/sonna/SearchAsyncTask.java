package org.sonna.www.sonna;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;

import org.sonna.www.sonna.services.BooksTreeNode;
import org.sonna.www.sonna.services.BooksTreeService;
import org.sonna.www.sonna.services.TextUtils;

import java.util.ArrayList;

public class SearchAsyncTask extends AsyncTask<Object, Void, String> {

    private final ProgressDialog dialog;
    private final ArrayList<String> list = new ArrayList<>();
    private final ArrayList<BooksTreeNode> curSearchHits = new ArrayList<>();
    private int totalHitsCount;
    private String searchWords;

    private final MainActivity mainActivity;
    private final View view;

    public SearchAsyncTask(MainActivity activity, View view) {
        this.mainActivity = activity;
        this.view = view;
        dialog = new ProgressDialog(activity);
    }

    protected void onPreExecute() {
        dialog.setMessage("جاري البحث...");
        dialog.show();
    }

    @Override
    protected String doInBackground(Object... params) {
        searchWords = (String) params[0];
        Integer pageNumber = (Integer) params[1];
        Integer pageLength = (Integer) params[2];
        BooksTreeService booksService = (BooksTreeService) params[3];

        //Lengthy Search Operation...
        totalHitsCount = booksService.getSearchHitsTotalCount("", searchWords);
        ArrayList<BooksTreeNode> hits = booksService.search(searchWords, pageLength, pageNumber);

        for (BooksTreeNode record : hits) {
            list.add(TextUtils.removeTrailingDot(record.getTitle()));
            curSearchHits.add(record);
        }

        return " ";
    }

    @Override
    protected void onPostExecute(String str) {
        //Update your UI here.... Get value from doInBackground ....
        if (dialog.isShowing()) {
            dialog.dismiss();
        }

        //Update search controls with search results
        mainActivity.updateSearchControls(view, searchWords, list, curSearchHits, totalHitsCount);
    }
}
