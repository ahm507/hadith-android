#! /usr/bin/env python
# coding: utf-8

import sqlite3
import re


#fix duplicate kitab in books level #L1

file_name = 'sonna.sqlite'

conn = sqlite3.connect(file_name)
cur = conn.cursor()
cur.execute('select * from pages where parent_id="0"')
records = cur.fetchall()
count = 0
print len(records)
for row in records:
	title = row[3]
	if type(title) is int:
		title = str(title)
	title = title.strip()
	parts = title.split(".")
	if len(parts) > 1 :
		title1 = parts[0].strip()
		title2 = parts[1].strip()
		if len(title2) > 0:
			# print title
			# print ">" + title1
			# print ">>" + title2
			count += 1
			# print title1, title2, row[0], row[2]
			# print "UPDATE pages SET title={0} and page={1} WHERE page_id={2} and book_code={3}".format(title1, title2, row[0], row[2]).encode("utf-8")
			# print "UPDATE pages SET title={0} and page={1} WHERE page_id={2} and book_code={3}" \
			# 	  + title1 +  title2 + row[0] + row[2]
			# print "UPDATE pages SET title={0} and page={1} WHERE page_id={2} and book_code={3}" \
			# 	  + title1 +  title2 + row[0] + row[2]

			# print query
			cur.execute(u"UPDATE pages SET title=?, page=? WHERE page_id=? and book_code=?", (title1, title2, row[0], row[2]))
			# cur.execute(u"select * from pages WHERE page_id=? and book_code=?", (row[0], row[2]))
			# records2 = cur.fetchall()
			
			# print u"an update is done for ID:" + str(row[0])
			# print "---"
			# print records2[0][3]
			#
			#
			# return

conn.commit()
conn.close()  # call basic function

print "total count of records:" + str(count)
