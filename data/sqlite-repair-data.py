#! /usr/bin/env python
# coding: utf-8


def repair_muslim_book_duplication(file_name):
	import sqlite3
	conn = sqlite3.connect(file_name)
	cur = conn.cursor()
	cur.execute('select * from pages where book_code = "g2b2" and parent_id = "0"')
	records = cur.fetchall()
	for row in records:
		title = row[3]
		import re
		m = re.match('[0-9]+ \- [^\d]*', title)
		title2 = m.group(0)
		print title
		print title2
		cur.execute("UPDATE pages SET title=? WHERE book_code ='g2b2' AND page_id=?",
            (title2, row[0]))

	conn.commit()
	conn.close()  # call basic function


def repair_muslim_hadith_title(file_name):
	print 'This function add Muslim title to the hadith items'
	import sqlite3
	conn = sqlite3.connect(file_name)
	cur = conn.cursor()
	cur.execute('select * from pages where book_code = "g2b2"')
	records = cur.fetchall()
	print 'Working to records,', len(records)
	for row in records:
		title = row[3]
		import re
		# hadith = 'حديث'.decode('utf-8')
		sahih_muslim =  'صحيح مسلم'.decode('utf-8')
		# m = re.match(hadith+' [0-9]+ ', title) #  $ end of text
		# m = re.match(sahih_muslim, title) #  $ end of text
		# hadith = 'صحيح مسلم'  #  .decode('utf-8')
		# m = re.match(hadith, title)
		# if m is None: #  not found
		if sahih_muslim not in title: #  not found
			title = row[3]
			print "id:", row[0]
			print "title:", title
			title2 = title + ' - صحيح مسلم'.decode('utf-8')
			print "title2:", title2
			cur.execute("UPDATE pages SET title=? WHERE book_code ='g2b2' AND page_id=?",
	            (title2, row[0]))

	print "committing to database and exiting"
	conn.commit()
	conn.close()


#############################

file_name = 'sonna-7b-compact.sqlite'

#This is a kitab duplication in Muslim
# repair_muslim_book_duplication(file_name)

#append Sahih Muslim string to leafes
# repair_muslim_hadith_title(file_name)
