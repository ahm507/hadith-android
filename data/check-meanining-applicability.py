#! /usr/bin/env python
# coding: utf-8

import sqlite3
import re

def find_empty_records(file_name):

	conn = sqlite3.connect(file_name)
	cur = conn.cursor()
	cur.execute('select * from pages')
	records = cur.fetchall()
	count = 0
	print "Total = " + str(len(records))

	meaning_sep = u"معانى بعض الكلمات"

	for row in records:
		content = row[4]
		content = content.strip()
		if content.find(meaning_sep) != -1:
			count = count + 1
			# print content

		# if count == 10:
		# 	return

	conn.close()  # call basic function
	print "Meanings records = " + str(count)

#############################

file_name = 'sonna.sqlite'

find_empty_records(file_name)
